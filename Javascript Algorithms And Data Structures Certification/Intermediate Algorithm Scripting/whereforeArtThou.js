function whatIsInAName(collection, source) {

  // What's in a name?
  let result = [];
  // Only change code below this line
  let keys = Object.keys(source);

  result = collection.filter( obj => keys.every( key => obj.hasOwnProperty(key) && obj[key] === source[key] ) ) ;

  // Only change code above this line 
  return result;
}