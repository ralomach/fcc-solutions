var Person = function(firstAndLast) {
  let fullName = firstAndLast;

  this.setFullName = function(name) {
    fullName = name;
    return fullName;
  }
  this.setFirstName = function(first) {
    fullName = first + " " + fullName.split(" ")[1];
    return fullName;
  }
  this.setLastName = function(last) {
    fullName = fullName.split(" ")[0] + " " + last;
    return fullName;
  }

  this.getFirstName = function() {
    return fullName.split(" ")[0];
  }
  this.getLastName = function() {
    return fullName.split(" ")[1];
  }
  this.getFullName = function() {
    return fullName;
  }  
};

var bob = new Person('Bob Ross');
bob.getFullName();