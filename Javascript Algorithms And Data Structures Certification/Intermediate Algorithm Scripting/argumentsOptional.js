function addTogether() {
  if ( arguments.length > 1 ) {
    if(Number.isInteger(arguments[0]) && Number.isInteger(arguments[1]) ) {
      return arguments[0] + arguments[1];
    } else {
      return undefined;
    }
    
  } else {
    
    if(Number.isInteger(arguments[0])){
      let arg = arguments[0];
      return function(arg2) {
        if ( Number.isInteger(arg) && Number.isInteger(arg2) ) {
          return arg + arg2;
        } 

      }

    }

  }

}