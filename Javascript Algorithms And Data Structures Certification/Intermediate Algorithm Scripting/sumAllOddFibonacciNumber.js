function sumFibs(num) {
  let result = 0;
  let arr = [1,1];

  if (num === 1) {
    return result = 1;
  } else {
      while ( arr[arr.length - 1] < num ) {
         arr.push( arr.reduce( (a, b, i, arr) => arr[i-1] + arr[i] ) );
       }        
    } 

  arr = arr.filter( a => a <= num && a % 2 !== 0 );

  result = arr.reduce( (a, b) => a + b );
  
  return result;
}