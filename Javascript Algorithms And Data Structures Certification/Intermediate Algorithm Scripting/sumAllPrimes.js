function sumPrimes(num) {
    let primes = [2, 3, 5, 7];
    let sum = 0;
  
    if (num > 1) {
      for (let i = 2; i <= num; i++) { 
        if ( primes.every( prime => i % prime !== 0 ) ) {
          primes.push(i);
        }
      }
    } else {
      console.log(`You number is ${num}. Please enter a number greater than 1`);
    }
  
    sum = primes.reduce( (a, b) => a + b );
  
    return sum;
}