function binaryAgent(str) {
    let result = str.split(" ");
  
    return result.map( item => String.fromCharCode(parseInt(item, 2) ) ).join("");
  }