function translatePigLatin(str) {
  let vogals = ["a","e","i","o","u"];
  let strLetters = [];
  let temp = [];

  for (let i of str) { strLetters.push(i); }
  
  if ( strLetters.every( letter => vogals.indexOf(letter) === -1 ) ) {
    strLetters.push("a", "y");
    str = strLetters.join("");
  } else {
    if ( vogals.indexOf(strLetters[0]) > -1 ) {
      strLetters.push("w", "a", "y");
      str = strLetters.join("");
    } else {
      let i = 0;
      while ( vogals.indexOf(strLetters[0]) === -1 ) {
        strLetters.push(strLetters[0]);
        strLetters.shift();
      }
      strLetters.push("a", "y");
      str = strLetters.join("");
    }
  }
  return str;  
}