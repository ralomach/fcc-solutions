function fearNotLetter(str) {
    let alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    let compare = str.split("");
    let i = alphabet.indexOf(compare[0]);
    let result;

    for (let j in compare) {
        if (compare[j] !== alphabet[i]) {
            result = alphabet[i];
            break;
        } 
        i++;
    }
    return result;
}