function steamrollArray(arr) {
  let result = [].concat(...arr);

  return result.some(Array.isArray) ? steamrollArray(result) : result;
}