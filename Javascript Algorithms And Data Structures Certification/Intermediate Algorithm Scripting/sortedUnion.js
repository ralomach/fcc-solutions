function uniteUnique(...arr) {
    let compare = [];
    let unique = [];
    for (let array of arr) {
      for (let i in array) {
        compare.push(array[i]);
      }
    }
  
    for (let item of compare) {
      if (unique.indexOf(item) === -1) { 
        unique.push(item);
      }
    }
    
    return unique;
}