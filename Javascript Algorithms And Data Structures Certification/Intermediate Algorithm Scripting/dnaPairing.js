function pairElement(str) {
    let strLetters = [];
  
    for (let letter of str) { strLetters.push([letter]) }
  
    for (let i in strLetters) {
      switch( strLetters[i][0] ) {
        case 'A':
          strLetters[i].push("T");
          break;
        case 'C':
          strLetters[i].push("G");
          break;
        case 'G':
          strLetters[i].push("C");
          break;
        case 'T':
          strLetters[i].push("A");
          break;
      }
    }
  
    return strLetters;
  }