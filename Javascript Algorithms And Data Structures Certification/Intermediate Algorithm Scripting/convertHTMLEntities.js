function convertHTML(str) {
    var MAP = { 
      '&': '&amp;', 
      '<': '&lt;', 
      '>': '&gt;', 
      '"': '&quot;', 
      "'": '&apos;'
    };
  
    return str.replace(/[&<>\"']/g, (entity) => MAP[entity] );
  }