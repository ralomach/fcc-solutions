function spinalCase(str) {
  str = str.replace(/_| | [A-Z]/g, "-");
  str = str.replace(/([a-z])([A-Z])/g, "$1-$2");
  str = str.toLowerCase();
  return str;
}