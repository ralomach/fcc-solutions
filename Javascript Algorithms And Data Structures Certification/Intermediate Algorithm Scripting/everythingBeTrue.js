function truthCheck(collection, pre) {
    return collection.every(item => item.hasOwnProperty(pre) && item[pre]);
}