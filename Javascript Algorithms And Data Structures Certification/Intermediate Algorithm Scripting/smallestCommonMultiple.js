function smallestCommons(arr) {
  let result = 0;
  let max = Math.max(...arr);
  let min = Math.min(...arr);
  let range = [];
  let scm = [];

  for (let i = min; i <= max; i++) { range.push(i); }

  while ( range.some( item => item !== 1 ) ) {
    if ( range.some( item => item % 2 === 0 ) ) {
      scm.push(2);
      range = range.map( item => {
        if ( item % 2 === 0 ) {
          return item / 2;
        } else {
          return item;
        }
      });
    } else if ( range.some( item => item % 3 === 0 ) ) {
      scm.push(3);
      range = range.map( item => {
        if ( item % 3 === 0 ) {
          return item / 3;
        } else {
          return item;
        }
      });
    } else if ( range.some( item => item % 5 === 0 ) ) {
      scm.push(5);
      range = range.map( item => {
        if ( item % 5 === 0 ) {
          return item / 5;
        } else {
          return item;
        }
      });
    } else {
      if ( range.some( item => item % item === 0 ) ) {
        range = range.map( item => {
            scm.push(item);
            return item / item;    
        });
      }
    }
  }

  result = scm.reduce( (a, b) => a * b );

  return result;
}