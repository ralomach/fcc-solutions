function orbitalPeriod(arr) {
    var GM = 398600.4418;
    var earthRadius = 6367.4447;
  
    for (let planet of arr) {
      let t = 2 * Math.PI * Math.sqrt( Math.pow( (earthRadius + planet.avgAlt), 3) / GM  );
      
      planet.orbitalPeriod = Math.round(t);
      delete planet.avgAlt;
    }
  
    return arr;
  }