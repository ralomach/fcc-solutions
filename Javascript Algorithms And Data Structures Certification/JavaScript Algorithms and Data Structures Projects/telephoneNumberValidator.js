function telephoneCheck(str) {
    const tester = /^1?\s?(\(\d{3}\)|\d{3})-?\s?\d{3}-?\s?(\d{4})$/;
    return tester.test(str);
}