function convertToRoman(num) {
    let result = [];

    while (num !== 0) {
        if( num % 1000 !== num ) {
            result.push("M");
            num -= 1000;
        } else if ( num % 500 !== num ) {
            result.push("D");
            num -= 500;
        } else if ( num % 100 !== num ) {
            result.push("C");
            num -= 100;
        } else if ( num % 50 !== num ) {
            result.push("L");
            num -= 50;
        } else if ( num % 10 !== num ) {
            result.push("X");
            num -= 10;
        } else if ( num % 5 !== num ) {
            result.push("V");
            num -= 5;
        } else {
            result.push("I");
            num -= 1;
        }
    }

    let str = result.join("");
    str = str.replace(/DCCCC/, "CM")
        .replace(/CCCC/, "CD")
        .replace(/LXXXX/, "XC")
        .replace(/XXXX/, "XL")
        .replace(/VIIII/, "IX")
        .replace(/IIII/, "IV")
    return str;
}