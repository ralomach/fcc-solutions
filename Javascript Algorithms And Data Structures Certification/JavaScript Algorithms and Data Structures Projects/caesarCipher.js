/*
A = 65, B = 66, C = 67, D = 68, E = 69, F = 70,
N = 78, O = 79, P = 80, Q = 81, R = 82, S = 83,

G = 71, H = 72, I = 73, J = 74, K = 75, L = 76, M = 77,
T = 84, U = 85, V = 86, W = 87, X = 88, Y = 89, Z = 90  
*/

function rot13(str) { 
    let upper = str.toUpperCase();
    let decoded = [];
    let codedChar = "";
    let char = "";
    let decodedChar = "";
    
    for (let i in upper){
        codedChar = upper[i].charCodeAt(0);
        if (codedChar >= 65 && codedChar <= 77) {
            char = upper[i].charCodeAt(0) + 13;
        } else if (codedChar >= 78 && codedChar <= 90) {
            char = upper[i].charCodeAt(0) - 13;
        } else {
            char = upper[i].charCodeAt(0);
        }
        decodedChar = String.fromCharCode(char);
        decoded.push(decodedChar);
    }
    
    let result = decoded.join("");

    return result;
}