function palindrome(str) {
    let model = str.toLowerCase().replace(/\W/gi, "").replace(/_/gi, "");
    
    let reversed = "";
    let array = [];
    
    for (let i in model) { array.unshift(model[i]) };
    
    reversed += array.join("");
    
    return model === reversed ? true : false;
}