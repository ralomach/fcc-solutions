function checkCashRegister(price, cash, cid) {
  let result = {status: "OPEN", change: []};
  let money = 0;
  let change = (cash * 100 - price * 100);
  let changeArr = [
    ["PENNY", 0],
    ["NICKEL", 0],
    ["DIME", 0],
    ["QUARTER", 0],
    ["ONE", 0],
    ["FIVE", 0],
    ["TEN", 0],
    ["TWENTY", 0],
    ["ONE HUNDRED", 0],
  ];

  cid = cid.map(item => [item[0], item[1] * 100]);
  
  for (let item of cid) {
    money += item[1];
  }

  if ( money < change ) {
    result = {
      status: "INSUFFICIENT_FUNDS",
      change: [],
    };
  } else if ( money === change ) {
    cid = cid.map(item => [item[0], item[1] / 100]);
    result = {
      status: "CLOSED",
      change: cid,
    }
  } else {
    while ( change > 0 ) {
      if ( change % 10000 !== change && cid[8][1] > 0 ) {
        change -= 10000;
        cid[8][1] -= 10000;
        changeArr[8][1] += 10000;
      } else if ( change % 2000 !== change && cid[7][1] > 0 ) {
        change -= 2000;
        cid[7][1] -= 2000;
        changeArr[7][1] += 2000;
      } else if ( change % 1000 !== change && cid[6][1] > 0 ) {
        change -= 1000;
        cid[6][1] -= 1000;
        changeArr[6][1] += 1000;
      } else if ( change % 500 !== change && cid[5][1] > 0 ) {
        change -= 500;
        cid[5][1] -= 500;
        changeArr[5][1] += 500;
      } else if ( change % 100 !== change && cid[4][1] > 0 ) {
        change -= 100;
        cid[4][1] -= 100;
        changeArr[4][1] += 100;
      } else if ( change % 25 !== change && cid[3][1] > 0 ) {
        change -= 25;
        cid[3][1] -= 25;
        changeArr[3][1] += 25;
      } else if ( change % 10 !== change && cid[2][1] > 0 ) {
        change -= 10;
        cid[2][1] -= 10;
        changeArr[2][1] += 10;
      } else if ( change % 5 !== change && cid[1][1] > 0 ) {
        change -= 5;
        cid[1][1] -= 5;
        changeArr[1][1] += 5;
      } else if ( change % 1 !== change && cid[0][1] > 0 ) {
        changeArr[0][1] += 1;
        cid[0][1] -= 1;
        change -= 1;
      } else {
        return result = {
          status: "INSUFFICIENT_FUNDS",
          change: [],
        };
      }
    }

    for (let i in changeArr) {
      if ( changeArr[i][1] > 0 ) {
        result.change.unshift([changeArr[i][0], changeArr[i][1]/100]);
      }
    }
  
  }
  
  return result;
}